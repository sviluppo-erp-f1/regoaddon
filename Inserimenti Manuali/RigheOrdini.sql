ALTER PROCEDURE RigheOrdini (
	IN in_DocEntry INT, 
	IN in_LineNum INT, 
	IN in_SoloData NVARCHAR(2),
	IN in_ItemCode NVARCHAR(100),
	IN in_MostraRes INT 
)
AS 

e_ID 			INT;
e_Qta			NUMERIC(19,6);
e_Continua 		NVARCHAR(2);
e_QtaRim		NUMERIC(19,6);
e_IDGiac		NVARCHAR(100);
e_IDOP 			INT ARRAY;
e_DocEntry 		INT;
e_DocNum		INT;
e_Date			INT;
e_QtaOP			INT;
e_QtaTrovata 	NUMERIC(19,6);
index_IDOP 		INT;
index_IDOP_FOR 	INT;
res_Fonte 		NVARCHAR(500);
res_GiacIniz	NUMERIC(19,6);
res_GiacFin		NUMERIC(19,6);
res_Data 		DATE;
res_QtaOp		NUMERIC(19,6);
BEGIN

-- CREO LE VARIE TABELLE DI APPOGGIO

--TMP_RDR1_ELAB -> VIENE POPOLATA INIZIALMENTE CON LA BASE DATI
--DROP TABLE TMP_RDR1_ELAB;
IF NOT EXISTS (select table_name from tables where table_name='TMP_RDR1_ELAB' and is_temporary='FALSE') THEN
	CREATE TABLE TMP_RDR1_ELAB(
		"ID"						INT,
		"ItemCode" 					NVARCHAR(100),
		"ItemName" 					NVARCHAR(254),
		"ShipDate" 					DATE,
		"DocEntry" 					INT,
		"DocNum" 					INT,
		"VisOrder" 					INT,
		"CardCode" 					NVARCHAR(50),
		"CardName" 					NVARCHAR(100),
		"Quantity" 					NUMERIC(19,6),
		"GiacIniziale" 				NUMERIC(19,6),
		"GiacFinale" 				NUMERIC(19,6),
		"QtaOp" 					NUMERIC(19,6),
		"Fonte" 					NVARCHAR(500),
		"DataConsegna" 				DATE,
		"OP"	 					INT,
		"Mag"	 					INT
	);
END IF;

--TMP_RDR1_GIACMAG -> VENGONO CARICATE TUTTE LE GIACENZE IN BASE ALLA BASE DATI
--DROP TABLE TMP_RDR1_GIACMAG;
IF NOT EXISTS (select table_name from tables where table_name='TMP_RDR1_GIACMAG' and is_temporary='FALSE') THEN
	CREATE TABLE TMP_RDR1_GIACMAG(
		"ID"						INT,
		"ItemCode" 					NVARCHAR(100),
		"GiacIniziale" 				NUMERIC(19,6),
		"GiacAttuale" 				NUMERIC(19,6)
	);
END IF;

--TMP_RDR1_ORDPROD -> VENGONO CARICATI TUTTI GLI ORDINI DI PRODUZIONE
--DROP TABLE TMP_RDR1_ORDPROD;
IF NOT EXISTS (select table_name from tables where table_name='TMP_RDR1_ORDPROD' and is_temporary='FALSE') THEN
	CREATE TABLE TMP_RDR1_ORDPROD(
		"ID"						INT,
		"ObjType"					NVARCHAR(100),
		"DocEntry"					INT,
		"DocNum"					INT,
		"Date"						DATE,
		"ItemCode" 					NVARCHAR(100),
		"QtaOP" 					NUMERIC(19,6),
		"QtaAttuale" 				NUMERIC(19,6)
	);
END IF;

--SVUOTO TUTTE LE TABELLE TEMPORANEE
TRUNCATE TABLE "TMP_RDR1_ELAB";
TRUNCATE TABLE "TMP_RDR1_GIACMAG";
TRUNCATE TABLE "TMP_RDR1_ORDPROD";

-- INSERISCO I DATI DA ELABOARARE SULLA TABELLA TEMPORANEA
INSERT INTO "TMP_RDR1_ELAB"
SELECT * FROM (
	SELECT 
		ROW_NUMBER() OVER (ORDER BY COALESCE(T0."ShipDate", T1."DocDueDate")) AS "ID",
		T0."ItemCode",
		T0A."ItemName",
		COALESCE(T0."ShipDate", T1."DocDueDate") AS "ShipDate",
		T1."DocEntry", 
		T1."DocNum", 
		T0."LineNum",
		T1."CardCode",
		T2."CardName",
		T0."Quantity", 
		NULL AS "GiacIniziale",
		NULL AS "GiacFinale",
		NULL AS "QtaOP",
		'-' AS "Fonte",
		NULL AS "DataConsegna",
		-- CARICO ANCHE LE QTA DEGLI OP PER CAPIRE SE DOPO DEVO ELABORARE LA RIGA
		CASE 
			WHEN COALESCE(T4."QtaOP",0) >= T0."Quantity" 
				THEN 1
			WHEN COALESCE(T4."QtaOP",0) > 0 
				THEN 2
			ELSE 0
		END AS "OP",
		-- CARICO ANCHE LE GIACENZE CAPIRE SE DOPO DEVO ELABORARE LA RIGA
		CASE 
			WHEN COALESCE(T3."GiacIniziale",0) >= T0."Quantity" 
				THEN 1
			WHEN COALESCE(T3."GiacIniziale",0) > 0
				THEN 2
			ELSE 0
		END AS "Mag"
	FROM 
		"RDR1" T0 
		INNER JOIN "OITM" T0A ON T0."ItemCode" = T0A."ItemCode"
		INNER JOIN "ORDR" T1 ON T0."DocEntry" = T1."DocEntry"
		INNER JOIN "OCRD" T2 ON T1."CardCode" = T2."CardCode" 
		LEFT JOIN
		( 
			SELECT 
				SUM(TA."OnHand") AS "GiacIniziale", 
				TA."ItemCode" 
			FROM "OITW" TA 
			GROUP BY TA."ItemCode"
		) T3 ON T0."ItemCode" = T3."ItemCode"
		LEFT JOIN 
		(
			SELECT 
				SUM(TB."PlannedQty") AS "QtaOP", 
				TB."ItemCode"
			FROM "OWOR" TB 
			WHERE TB."Status"  = 'R' AND TB."PlannedQty" < TB."CmpltQty"
			GROUP BY TB."ItemCode"
		) T4 ON T0."ItemCode" = T4."ItemCode"
	WHERE T0."OpenQty" > 0 AND T1."DocStatus" = 'O' AND T1."CANCELED" = 'N' AND T1.U_FO_PORD = 'S' AND T0A."InvntItem" = 'Y'
	ORDER BY T0."ShipDate"
);

-- POPOLO LA TABELLA DELLE GIACENZE DI MAGAZZINO INIZIALI RAGGRUPPATE PER ARTICOLI
INSERT INTO "TMP_RDR1_GIACMAG"
SELECT * FROM (
	SELECT 
		ROW_NUMBER() OVER (ORDER BY TA."ItemCode") AS "ID",
		TA."ItemCode",
		TB."Giacenza", 
		TB."Giacenza"
	FROM 
		"TMP_RDR1_ELAB" TA 
		INNER JOIN (
			SELECT 
				SUM(TA1."OnHand") AS "Giacenza", 
				TA1."ItemCode" 
			FROM "OITW" TA1
			GROUP BY TA1."ItemCode"
		) TB ON TA."ItemCode" = TB."ItemCode"
	WHERE 
		TA."Mag" <> 0
	GROUP BY TA."ItemCode", TB."Giacenza"
);

--POPOLO LA TABELLA DEGLI ORDINI DI PRODUZIONE E LE QTA
INSERT INTO "TMP_RDR1_ORDPROD" 
SELECT	ROW_NUMBER() OVER (ORDER BY "Data") AS "ID",*	
FROM (
	SELECT 
		'OP' as "ObjType",
		TA."DocEntry",
		TA."DocNum",
		COALESCE(TA."U_FO_FP_DTFINPRODPL", TA."DueDate") AS "Data",
		TA."ItemCode",
		TA."PlannedQty",
		TA."PlannedQty"
	FROM 
		"OWOR" TA 
		INNER JOIN (
			SELECT 
				DISTINCT (TB1."ItemCode") AS "ItemCode"  
			FROM "TMP_RDR1_ELAB" TB1 
			WHERE TB1."OP" <> 0 
		) TB ON TA."ItemCode" = TB."ItemCode"
	WHERE 
		TA."Status" = 'R' AND ADD_DAYS(TA."DueDate",365) > CURRENT_DATE AND TA."PlannedQty" < TA."CmpltQty"
	UNION ALL
	SELECT 
		'OA' as "ObjType",
		TA."DocEntry",
		TA."DocNum",
		COALESCE(TA."U_FO_FP_DTFINPRODPL", TA."DueDate") AS "Data",
		TA."ItemCode",
		TA."PlannedQty",
		TA."PlannedQty"
	FROM 
		(
			SELECT 	a."DocNum",
					a."DocEntry", 
					b."ItemCode",
					b."ShipDate" as "DueDate" , 
					b."Quantity" as "PlannedQty",
					NULL AS "U_FO_FP_DTFINPRODPL",
					b."LineStatus" as "Status"
			FROM 	OPOR a 
						INNER JOIN POR1 b ON a."DocEntry" = b."DocEntry"
			WHERE	a.U_FO_PORD = 'S'
		) TA 
		INNER JOIN (
			SELECT 
				DISTINCT (TB1."ItemCode") AS "ItemCode"  
			FROM "TMP_RDR1_ELAB" TB1 
			WHERE TB1."OP" <> 0
		) TB ON TA."ItemCode" = TB."ItemCode"
	WHERE 
		TA."Status" = 'O' AND ADD_DAYS(TA."DueDate",365) > CURRENT_DATE	
);


-- DICHIARO IL CURSORE SOLO PER LE RIGHE DA ELABORARE NUOVAMENTE
BEGIN
	DECLARE CURSOR  cur_ela  FOR
		SELECT	
			"ID", "Quantity", "ItemCode"
		FROM "TMP_RDR1_ELAB"
		WHERE "Mag" > 0 OR "OP" > 0 
		ORDER BY "ID";
	-- CICLO SULLE RIGHE DEL CURSORE ED ELABORO OGNI SINGOLA RIGA
	FOR cur_ela_row AS cur_ela DO 

		-- SETTO LE VARIABILI 
		e_Continua := 'Y';
		res_Fonte := '';
		res_Data := NULL;
		res_GiacFin := 0;
		res_GiacIniz := 0;
		e_QtaOP := 0;
		e_QtaRim := 0;
		e_IDGiac := '';
		e_IDOP := ARRAY();
		index_IDOP := 1;
		res_QtaOp := 0;

		--CONTROLLO SE HO GIACENZE IN MAGAZZINO
		SELECT T0."ID", T0."GiacAttuale" 
		INTO e_ID, e_Qta
		DEFAULT 0, 0
		FROM "TMP_RDR1_GIACMAG" T0 
		WHERE T0."ItemCode" = cur_ela_row."ItemCode" AND T0."GiacAttuale" > 0;

		e_QtaRim := cur_ela_row."Quantity";

		-- SE LA GIACENZA COPRE INTERAMENTE LA RIGA
		IF :e_Qta >= :e_QtaRim  THEN 
			e_Continua := 'N';
			res_GiacFin := :e_Qta - :e_QtaRim;
			res_Fonte := 'Magazzino';
			res_Data := CURRENT_DATE;
			res_GiacIniz := e_Qta;

			-- AGGIORNO LA TABELLA DI APPOGGIO CON LA NUOVA QTA 
			UPDATE "TMP_RDR1_GIACMAG" 
			SET "GiacAttuale" = :res_GiacFin
			WHERE "ID" = :e_ID;

		-- SE LA QTA E' Maggiore di 0 ma non sufficiente per evadere completamente una riga 
		-- scalo la qta e mi segno l'id in modo da aggiornare la qta in fondo all'elaborazione
		ELSEIF :e_Qta > 0 THEN
			res_GiacFin := 0;
			e_QtaRim := :e_QtaRim - :e_Qta;
			res_Fonte := 'Magazzino';
			res_GiacIniz := :e_Qta;
			e_IDGiac := :e_ID;
		END IF;

		-- Se non ho coperto la qta con le giacenze controllo gli OP
		IF :e_Continua = 'Y' THEN 
			BEGIN
				-- DICHIARO un cursore in modo da ciclare tutti gli OP salvati prima
				DECLARE CURSOR  cur_op  FOR
					SELECT T0."ID",T0."ObjType", T0."DocEntry", T0."DocNum", T0."Date", T0."QtaAttuale"
					FROM "TMP_RDR1_ORDPROD" T0 
					WHERE T0."ItemCode" = cur_ela_row."ItemCode" AND T0."QtaAttuale" > 0
					ORDER BY ID;

				FOR cur_op_row AS cur_op DO 
					IF :e_Continua = 'Y' THEN 
						e_Qta := cur_op_row."QtaAttuale";

						-- SE LA QTA DELL'OP E' SUFFICIENTE PER CHIUDERE LA RIGA
						IF e_Qta >= :e_QtaRim THEN 
							e_Continua := 'N';
							res_Fonte := :res_Fonte||' '||cur_op_row."ObjType"||'-'||cur_op_row."DocNum";
							res_Data := cur_op_row."Date";
							res_QtaOp := :res_QtaOp + :e_QtaRim;
							e_QtaTrovata := :e_Qta - :e_QtaRim;

							-- AGGIORNO LA RIGA SULLA TAB UTENTE
							UPDATE "TMP_RDR1_ORDPROD" 
							SET "QtaAttuale" = :e_QtaTrovata
							WHERE "ID" =  cur_op_row."ID";

						-- SE LA QTA NON COPRE TOTALMENTE LA RIGA MA LA COPRE PARZIALMENTE 
						-- COME PER LE GIACENZE MI SEGNO GLI ID E SCALO LA QTA RIMANENTE
						ELSEIF e_Qta > 0 THEN
							e_QtaRim := :e_QtaRim - :e_Qta;
							res_Fonte := :res_Fonte||' '||cur_op_row."ObjType"||'-'||cur_op_row."DocNum";
							res_Data := cur_op_row."Date";
							res_QtaOp := :res_QtaOp + :e_Qta;
							e_IDOP[index_IDOP] := cur_op_row."ID";
							index_IDOP := :index_IDOP + 1;
						END IF;
						IF LENGTH(:res_Fonte) > 500 THEN 
							res_Fonte := SUBSTRING(:res_Fonte, 0, 499);
						END IF;

					END IF;
				END FOR;
			END;
		END IF; 

		-- CONTROLLO SE HO RIGHE DA AGGIORNARE NELLE TABELLE DI APPOGGIO
		IF :e_Continua = 'N' THEN 
			-- SE HO DEGLI OP DA AGGIORNARE LE QTA
			FOR index_IDOP_FOR IN 1 .. :index_IDOP DO
				UPDATE "TMP_RDR1_ORDPROD" 
				SET "QtaAttuale" = 0 
				WHERE "ID" IN (:e_IDOP[index_IDOP_FOR]);
			END FOR;

			-- SE HO GIACENZE DA AGGIORNARE (NE POSSO AVERE UNA SOLA)
			IF :e_IDGiac != '' THEN 
				UPDATE "TMP_RDR1_GIACMAG" 
				SET "GiacAttuale" = 0
				WHERE "ID" = :e_IDGiac;
			END IF;
		END IF;

		-- AGGIORNO LA TABELLA DI OUTPUT
		IF :res_Data IS NULL THEN 
			res_Fonte := '-';
		END IF;
		UPDATE "TMP_RDR1_ELAB" 
		SET 
			"DataConsegna" = :res_Data, 
			"Fonte" = :res_Fonte, 
			"GiacIniziale" = :res_GiacIniz, 
			"GiacFinale" = :res_GiacFin,
			"QtaOp" = :res_QtaOp
		WHERE "ID" = cur_ela_row."ID"; 

	END FOR;
END;


--------
		UPDATE "TMP_RDR1_ELAB" TA SET
			TA."DataConsegna" = ADD_DAYS(COALESCE(TA."DataConsegna",CURRENT_DATE), COALESCE(TC.U_FO_GIORNI,0))

		FROM "TMP_RDR1_ELAB" TA
				INNER JOIN OITM TB ON TA."ItemCode" = TB."ItemCode"
				LEFT OUTER JOIN "@FO_GG_FAMPROD" TC ON TB."U_FamProd" = TC.U_FO_FAMPROD		
		WHERE 
			(in_DocEntry = 0 OR TA."DocEntry" = :in_DocEntry ) 
			AND (in_LineNum = 0 OR TA."VisOrder" = :in_LineNum )
			AND (in_ItemCode IS NULL OR TA."ItemCode" = :in_ItemCode );
		
		
--------

-- PREPARO L'OUTPUT CON I DATI CHE MI ARRIVANO DALLA TESTATA E FORMATTO 
IF :in_MostraRes = 1 THEN 
	IF :in_SoloData = 'N' THEN 
		SELECT 
			TA."ItemCode" AS "Codice Articolo",
			TA."ShipDate" AS "Data Consegna Ordine",
			TA."DocNum" AS "Numero Ordine",
			TA."DocEntry" AS "Codice interno",
			TA."VisOrder" AS "Riga Ordine",
			TA."CardCode" AS "Codice Cliente",
			TA."CardName" AS "Cliente",
			TA."Quantity" AS "Quantita Ordine",
			COALESCE(TA."GiacIniziale",0) AS "Giacenza Iniziale",
			COALESCE(TA."GiacFinale",0) AS "Giacenza Finale",
			COALESCE(TA."QtaOp",0) AS "Quantita da Ordine Produzione",
			TRIM(TA."Fonte") AS "Fonte",
			--ADD_DAYS(TA."DataConsegna", COALESCE(TC.U_FO_GIORNI,0)) AS "Data Merce Pronta"
			TA."DataConsegna"  AS "Data Merce Pronta"
		FROM "TMP_RDR1_ELAB" TA
				INNER JOIN OITM TB ON TA."ItemCode" = TB."ItemCode"
				LEFT OUTER JOIN "@FO_GG_FAMPROD" TC ON TB."U_FamProd" = TC.U_FO_FAMPROD
		WHERE 
			(in_DocEntry = 0 OR TA."DocEntry" = :in_DocEntry ) 
			AND (in_LineNum = 0 OR TA."VisOrder" = :in_LineNum )
			AND (in_ItemCode IS NULL OR TA."ItemCode" = :in_ItemCode )
		ORDER BY TA."ID";
		
	ELSEIF :in_SoloData = 'Y' THEN
		SELECT TA."DataConsegna"  AS "Data Merce Pronta"
			--ADD_DAYS(TA."DataConsegna", COALESCE(TC.U_FO_GIORNI,0)) AS "Data"
		FROM "TMP_RDR1_ELAB" TA
				INNER JOIN OITM TB ON TA."ItemCode" = TB."ItemCode"
				LEFT OUTER JOIN "@FO_GG_FAMPROD" TC ON TB."U_FamProd" = TC.U_FO_FAMPROD		
		WHERE 
			(in_DocEntry = 0 OR TA."DocEntry" = :in_DocEntry ) 
			AND (in_LineNum = 0 OR TA."VisOrder" = :in_LineNum )
			AND (in_ItemCode IS NULL OR TA."ItemCode" = :in_ItemCode )
		ORDER BY TA."ID";
	END IF;
END IF;
END

ALTER Procedure prcReport (v_sequence  nvarchar(100))
As 

   v_CodiceBP					nvarchar(100);
   v_NumeroOrdine				nvarchar(100);
   v_IDinternoDocumento		nvarchar(100);
   v_NomeBP					nvarchar(1000);
   v_QtaRiga					numeric(19,6);
   v_Articolo					nvarchar(100);
   v_cont						INT;
   v_Ordine					INT;
   v_Code						INT;
BEGIN

IF NOT EXISTS (select table_name from tables where table_name='TMP_DIBA' and is_temporary='TRUE') THEN
		
	  CREATE GLOBAL TEMPORARY TABLE TMP_DIBA(
	"Ordine"						INT,
	"Gruppo"						nvarchar(100),
    "Articolo"					nvarchar(100),
    "Descrizione"					nvarchar(100),
    "QuantitàTotale"				numeric(19,6),
    "QuantitàGiacenza"			numeric(19,6),
    "Magazzino"					nvarchar(100),
    "Ubicazione"					nvarchar(100),
    "CodiceBP"					nvarchar(100),
    "NumeroOrdine"				nvarchar(100),
    "IDinternoDocumento"			INT,
    "NomeBP"						nvarchar(100)

);
END IF;

v_Ordine:=0;

BEGIN
DECLARE CURSOR  cur_temp  for
	select	"Code",
			U_FO_ARTICOLO,
			U_FO_BP,
			U_FO_DOCENTRY,
			U_FO_NOME_BP,
			U_FO_NUM_ORD,
			U_FO_QNT_RIGA
	from "@FO_ARTICOLO_BP"
	where	"Name"=v_sequence 
	order by "Code";

open cur_temp;

fetch cur_temp into v_Code, v_Articolo,v_CodiceBP,v_IDinternoDocumento,v_NomeBP,v_NumeroOrdine,v_QtaRiga;
WHILE not cur_temp::NOTFOUND DO


    v_Ordine = v_Ordine+1;

	select   count(*) INTO v_cont from oitt T0 where T0."Code" = :v_Articolo;
	if :v_cont=0 then
		
			insert into TMP_DIBA 
			select	v_Ordine,
					T2."ItmsGrpNam" as "Gruppo articolo", 
					T0."ItemCode" as "Articolo", 
					T0."ItemName" as "Descrizione", 
					1 * v_QtaRiga as "Quantità totale Necessaria" ,  
					t3."OnHand" as "Quantità in giacenza", 
					t3."WhsCode" as "Magazzino", 
					T8."BinCode" as "Ubicazione", 
					v_CodiceBP as "Codice BP", 
					v_NumeroOrdine as "Numero Ordine", 
					v_IDinternoDocumento AS "ID interno documento", 
					v_NomeBP AS "Nome BP"    
			FROM	OITM T0 
						INNER JOIN OITB T2 ON T2."ItmsGrpCod" = T0."ItmsGrpCod"  
						INNER JOIN OITW T3 ON T3."ItemCode" = T0."ItemCode"  AND T3."WhsCode" = 'Mc'
						LEFT JOIN OIBQ T7 ON T0."ItemCode" = T7."ItemCode" AND T7."OnHandQty" > 0   
						LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs"   
			WHERE	T0."ItemCode" =  :v_Articolo AND
					NOT EXISTS ( SELECT 1 FROM OITT T1 WHERE T0."ItemCode" = T1."Code" ) ; 
		end if;
	
	if v_cont!=0 THEN

		
			insert into TMP_DIBA 
			SELECT * FROM (						
			WITH cte AS(						
			SELECT
			CAST(parent_id AS varchar(50)) AS "parent"
			,CAST(node_id AS varchar(50)) AS "child"
			,hierarchy_level-1 AS "level"
			,CAST(Qty AS decimal(19,6)) AS "Qauntity"
			,"Warehouse" as "ToWH"
			FROM HIERARCHY ( SOURCE (
			SELECT CAST("Code" AS varchar(50)) AS node_id
			,CAST(null AS varchar(50)) AS parent_id
			,CAST("Qauntity" AS decimal(19,6)) AS Qty
			,"ToWH" as "Warehouse"
			FROM OITT
			WHERE "Code"=:v_Articolo
			UNION ALL

			SELECT "Code" as node_id
			,"Father" AS parent_id
			,"Quantity" AS Qty
			,"Warehouse" AS "Warehouse"
			FROM ITT1
			)
			CACHE FORCE
			))
			,
			albero as
				(
					SELECT	t0."child" "Articolo" , t0."Qauntity" ,t0."ToWH", t0."child" L0, null L1, null L2, null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0
					where t0."level"=0
				union 
					SELECT	t1."child" "Articolo", t1."Qauntity", t1."ToWH", t0."child" L0, t1."child" L1, null L2, null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1 
					where t0."level"=0
					and   t1."level"=1
					and   t1."parent" =t0."child"
				union 
					SELECT	t2."child" "Articolo", t2."Qauntity", t2."ToWH", t0."child" L0,t1."child" L1, t2."child" L2 , null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
				union 
					SELECT t3."child" "Articolo", t3."Qauntity", t3."ToWH", t0."child" L0,t1."child" L1, t2."child" L2, t3."child" L3, null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3 
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
				union 
					SELECT t4."child" "Articolo", t4."Qauntity", t4."ToWH", t0."child" L0, t1."child" L1, t2."child" L2, t3."child" L3, t4."child" L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
			union
					SELECT	t5."child" "Articolo", t5."Qauntity", t5."ToWH", t0."child" L0, t1."child" L1, t2."child" L2, t3."child" L3, t4."child" L4, t5."child" L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t5."level"=5
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
					and   t5."parent" =t4."child"
				union 
					SELECT	t6."child" "Articolo", t6."Qauntity", t6."ToWH", t0."child" L0, t1."child" L1, t2."child" L2, t3."child" L3, t4."child" L4, t5."child" L5, t6."child" L6, null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t5."level"=5
					and   t6."level"=6
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
					and   t5."parent" =t4."child"
					and   t6."parent" =t5."child"
				union 
					SELECT	t7."child" "Articolo", t7."Qauntity", t7."ToWH", t0."child" L0, t1."child" L1, t2."child" L2,  t3."child" L3 , t4."child" L4, t5."child" L5, t6."child" L6, t7."child" L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t5."level"=5
					and   t6."level"=6
					and   t7."level"=7
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
					and   t5."parent" =t4."child"
					and   t6."parent" =t5."child"
					and   t7."parent" =t6."child"
				union 
					SELECT t8."child" "Articolo", t8."Qauntity", t8."ToWH", t0."child" L0, t1."child" L1, t2."child" L2,  t3."child" L3, t4."child" L4, t5."child" L5, t6."child" L6, t7."child" L7, t8."child" L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7, CTE t8
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t5."level"=5
					and   t6."level"=6
					and   t7."level"=7
					and   t8."level"=8
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
					and   t5."parent" =t4."child"
					and   t6."parent" =t5."child"
					and   t7."parent" =t6."child"
					and   t8."parent" =t7."child"
				union
					SELECT	t9."child" "Articolo", t9."Qauntity", t9."ToWH", t0."child" L0, t1."child", t2."child",  t3."child" , t4."child", t5."child" , t6."child" , t7."child", t8."child",t9."child"
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7, CTE t8, CTE t9
					where t0."level"=0
					and   t1."level"=1
					and   t2."level"=2
					and   t3."level"=3
					and   t4."level"=4
					and   t5."level"=5
					and   t6."level"=6
					and   t7."level"=7
					and   t8."level"=8
					and   t9."level"=9
					and   t1."parent" =t0."child"
					and   t2."parent" =t1."child"
					and   t3."parent" =t2."child"
					and   t4."parent" =t3."child"
					and   t5."parent" =t4."child"
					and   t6."parent" =t5."child"
					and   t7."parent" =t6."child"
					and   t8."parent" =t7."child"
					and   t9."parent" =t8."child" 
			) 
					SELECT	v_Ordine,
							T2."ItmsGrpNam" as "Gruppo articolo", 
 							T0."Articolo" as "Articolo", 
 							T1."ItemName" as "Descrizione", 
 							T0."Qauntity" * v_QtaRiga  as "Quantità totale Necessaria" , 
 							t3."OnHand" as "Quantità in giacenza", 
 							t3."WhsCode" as "Magazzino", 
 							T8."BinCode" as "Ubicazione", 
							v_CodiceBP as "Codice BP", 
							v_NumeroOrdine as "Numero Ordine", 
							v_IDinternoDocumento AS "ID interno documento", 
							v_NomeBP AS "Nome BP"  
					FROM	ALBERO T0 
								INNER JOIN OITM T1 ON T1."ItemCode" = T0."Articolo" 
 								INNER JOIN OITB T2 ON T2."ItmsGrpCod" = T1."ItmsGrpCod"  
 								INNER JOIN OITW T3 ON T3."ItemCode" = T1."ItemCode" AND T3."WhsCode" = 'Mc'
 								LEFT JOIN OIBQ T7 ON T1."ItemCode" = T7."ItemCode" AND T7."OnHandQty" > 0 and T7."WhsCode" = T3."WhsCode"  
 								LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs" and T7."WhsCode" = T3."WhsCode" 
					order by  L0,L1,L2,L3,L4,L5,L6,L7,L8,L9			);	
		end IF;
fetch cur_temp into v_Code, v_Articolo,v_CodiceBP,v_IDinternoDocumento,v_NomeBP,v_NumeroOrdine,v_QtaRiga;
END WHILE; 

close cur_temp; 
END;
select	  
	a."Gruppo",
    a."Articolo",
    a."Descrizione",
    a."QuantitàTotale",
    a."QuantitàGiacenza",
    a."Magazzino",
    a."Ubicazione",
    a."CodiceBP",
    a."NumeroOrdine",
    a."IDinternoDocumento",
    a."NomeBP"
from TMP_DIBA a;

END


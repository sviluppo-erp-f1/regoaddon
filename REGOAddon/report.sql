USE [REGO_PROD]
GO
/****** Object:  StoredProcedure [dbo].[[prcReport]]    Script Date: 17/12/2021 15:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec dbo.PR_BudgetAgenti 2020,'61'
ALTER Procedure [dbo].[prcReport](@sequence  nvarchar(100))
As 

  declare @CodiceBP					nvarchar(100)
  declare @NumeroOrdine				nvarchar(100)
  declare @IDinternoDocumento		nvarchar(100)
  declare @NomeBP					nvarchar(1000)
  declare @QtaRiga					numeric(19,6)
  declare @Articolo					nvarchar(100)
  declare @cont						INT
  declare @Ordine					Int
  declare @Code						INT
BEGIN

  CREATE TABLE #TMP_DIBA(
	Ordine						int,
	Gruppo						nvarchar(100),
    Articolo					nvarchar(100),
    Descrizione					nvarchar(100),
    QuantitąTotale				numeric(19,6),
    QuantitąGiacenza			numeric(19,6),
    Magazzino					nvarchar(100),
    Ubicazione					nvarchar(100),
    CodiceBP					nvarchar(100),
    NumeroOrdine				nvarchar(100),
    IDinternoDocumento			INT,
    NomeBP						nvarchar(100)

)


	set @Ordine=0

declare  cur_temp cursor for
	select	Code,
			U_FO_ARTICOLO,
			U_FO_BP,
			U_FO_DOCENTRY,
			U_FO_NOME_BP,
			U_FO_NUM_ORD,
			U_FO_QNT_RIGA
	from "@FO_ARTICOLO_BP"
	where	Name=@sequence 
	order by Code

open cur_temp

fetch next from cur_temp into @Code, @Articolo,@CodiceBP,@IDinternoDocumento,@NomeBP,@NumeroOrdine,@QtaRiga
while @@fetch_status=0
begin

	set @Ordine = @Ordine+1

	select @cont = count(*) from oitt T0 where T0.Code = @Articolo
	if @cont=0
		begin
			insert into #TMP_DIBA 
			select	@Ordine,
					T2.ItmsGrpNam as "Gruppo articolo", 
					T0.ItemCode as "Articolo", 
					T0.ItemName as "Descrizione", 
					1 * @QtaRiga as "Quantitą totale Necessaria" ,  
					t3.OnHand as "Quantitą in giacenza", 
					t3.WhsCode as "Magazzino", 
					T8.BinCode as "Ubicazione", 
					@CodiceBP as "Codice BP", 
					@NumeroOrdine as "Numero Ordine", 
					@IDinternoDocumento AS "ID interno documento", 
					@NomeBP AS "Nome BP"    
			FROM	OITM T0 
						INNER JOIN OITB T2 ON T2.ItmsGrpCod = T0.ItmsGrpCod  
						INNER JOIN OITW T3 ON T3.ItemCode = T0.ItemCode  AND T3.WhsCode = 'Mc'
						LEFT JOIN OIBQ T7 ON T0.ItemCode = T7.ItemCode AND T7.OnHandQty > 0   
						LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs"   
			WHERE	T0.ItemCode =  @Articolo AND
					NOT EXISTS ( SELECT 1 FROM OITT T1 WHERE T0.ItemCode = T1.Code )  
		end
	
	if @cont!=0
		begin

			WITH cte AS
					(
						SELECT	distinct NULL as parent, 
								T0.Code as child, 
								0 as level ,
								T0.Qauntity,
								T0.ToWH
						FROM	OITT T0 
						WHERE	T0.Code= @Articolo
						UNION ALL
						SELECT	T0.Father as parent, 
								T0.Code as child, 
								1 as level ,
								T0.Quantity,
								T0.Warehouse
						FROM	ITT1 T0 
						WHERE	T0.Father=@Articolo
						UNION ALL
						SELECT	T0.Father, 
								T0.Code , 
								(c.level + 1),
								T0.Quantity,
								T0.Warehouse
						FROM	ITT1 T0  
									INNER JOIN cte c ON T0.Father = c.child
						WHERE	c.parent is not  null
						)  
						,
			albero as
				(
					SELECT	t0.child articolo , t0.Qauntity ,t0.ToWH, t0.child L0, null L1, null L2, null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0
					where t0.level=0
				union 
					SELECT	t1.child articolo, t1.Qauntity, t1.ToWH, t0.child L0, t1.child L1, null L2, null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1 
					where t0.level=0
					and   t1.level=1
					and   t1.parent =t0.child
				union 
					SELECT	t2.child articolo, t2.Qauntity, t2.ToWH, t0.child L0,t1.child L1, t2.child L2 , null L3 , null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t1.parent =t0.child
					and   t2.parent =t1.child
				union 
					SELECT t3.child articolo, t3.Qauntity, t3.ToWH, t0.child L0,t1.child L1, t2.child L2, t3.child L3, null L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3 
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
				union 
					SELECT t4.child articolo, t4.Qauntity, t4.ToWH, t0.child L0, t1.child L1, t2.child L2, t3.child L3, t4.child L4, null L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
			union
					SELECT	t5.child articolo, t5.Qauntity, t5.ToWH, t0.child L0, t1.child L1, t2.child L2, t3.child L3, t4.child L4, t5.child L5, null L6 , null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t5.level=5
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
					and   t5.parent =t4.child
				union 
					SELECT	t6.child articolo, t6.Qauntity, t6.ToWH, t0.child L0, t1.child L1, t2.child L2, t3.child L3, t4.child L4, t5.child L5, t6.child L6, null L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t5.level=5
					and   t6.level=6
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
					and   t5.parent =t4.child
					and   t6.parent =t5.child
				union 
					SELECT	t7.child articolo, t7.Qauntity, t7.ToWH, t0.child L0, t1.child L1, t2.child L2,  t3.child L3 , t4.child L4, t5.child L5, t6.child L6, t7.child L7, null L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t5.level=5
					and   t6.level=6
					and   t7.level=7
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
					and   t5.parent =t4.child
					and   t6.parent =t5.child
					and   t7.parent =t6.child
				union 
					SELECT t8.child articolo, t8.Qauntity, t8.ToWH, t0.child L0, t1.child L1, t2.child L2,  t3.child L3, t4.child L4, t5.child L5, t6.child L6, t7.child L7, t8.child L8, null L9
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7, CTE t8
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t5.level=5
					and   t6.level=6
					and   t7.level=7
					and   t8.level=8
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
					and   t5.parent =t4.child
					and   t6.parent =t5.child
					and   t7.parent =t6.child
					and   t8.parent =t7.child
				union
					SELECT	t9.child articolo, t9.Qauntity, t9.ToWH, t0.child L0, t1.child, t2.child,  t3.child , t4.child, t5.child , t6.child , t7.child, t8.child,t9.child
					FROM	CTE t0, CTE t1, CTE t2, CTE t3, CTE t4, CTE t5, CTE t6, CTE t7, CTE t8, CTE t9
					where t0.level=0
					and   t1.level=1
					and   t2.level=2
					and   t3.level=3
					and   t4.level=4
					and   t5.level=5
					and   t6.level=6
					and   t7.level=7
					and   t8.level=8
					and   t9.level=9
					and   t1.parent =t0.child
					and   t2.parent =t1.child
					and   t3.parent =t2.child
					and   t4.parent =t3.child
					and   t5.parent =t4.child
					and   t6.parent =t5.child
					and   t7.parent =t6.child
					and   t8.parent =t7.child
					and   t9.parent =t8.child 
			) 
					insert into #TMP_DIBA 
					SELECT	@Ordine,
							T2.ItmsGrpNam as "Gruppo articolo", 
 							T0.articolo as "Articolo", 
 							T1.ItemName as "Descrizione", 
 							T0.Qauntity * @QtaRiga  as "Quantitą totale Necessaria" , 
 							t3.OnHand as "Quantitą in giacenza", 
 							t3.WhsCode as "Magazzino", 
 							T8.BinCode as "Ubicazione", 
							@CodiceBP as "Codice BP", 
							@NumeroOrdine as "Numero Ordine", 
							@IDinternoDocumento AS "ID interno documento", 
							@NomeBP AS "Nome BP"  
					FROM	ALBERO T0 
								INNER JOIN OITM T1 ON T1.ItemCode = T0.articolo 
 								INNER JOIN OITB T2 ON T2.ItmsGrpCod = T1.ItmsGrpCod  
 								INNER JOIN OITW T3 ON T3.ItemCode = T1.ItemCode AND T3.WhsCode = 'Mc'
 								LEFT JOIN OIBQ T7 ON T1.ItemCode = T7.ItemCode AND T7.OnHandQty > 0 and T7.WhsCode = T3.WhsCode  
 								LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs" and T7."WhsCode" = T3."WhsCode" 
					order by  L0,L1,L2,L3,L4,L5,L6,L7,L8,L9				
		end
	fetch next from cur_temp into @Code, @Articolo,@CodiceBP,@IDinternoDocumento,@NomeBP,@NumeroOrdine,@QtaRiga   
end

close cur_temp
deallocate cur_temp 

select	  
	a.Gruppo,
    a.Articolo,
    a.Descrizione,
    a.QuantitąTotale,
    a.QuantitąGiacenza,
    a.Magazzino,
    a.Ubicazione,
    a.CodiceBP,
    a.NumeroOrdine,
    a.IDinternoDocumento,
    a.NomeBP
from #TMP_DIBA a

END


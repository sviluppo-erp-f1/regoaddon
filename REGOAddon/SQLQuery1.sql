USE [REGO_PROD]
GO
/****** Object:  StoredProcedure [dbo].[prcEsplodiDiba]    Script Date: 17/12/2021 15:44:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec dbo.PR_BudgetAgenti 2020,'61'
ALTER Procedure [dbo].[prcEsplodiDiba](@sequence  nvarchar(100))
As 

  declare @CodiceBP					nvarchar(100)
  declare @NumeroOrdine				nvarchar(100)
  declare @IDinternoDocumento		nvarchar(100)
  declare @NomeBP					nvarchar(1000)
  declare @QtaRiga					numeric(19,6)
  declare @Articolo					nvarchar(100)
  declare @cont						INT
  declare @Ordine					Int
  declare @Code						INT
BEGIN

  CREATE TABLE #TMP_DIBA(
	Ordine						int,
	Gruppo						nvarchar(100),
    Articolo					nvarchar(100),
    Descrizione					nvarchar(100),
    QuantitąTotale				numeric(19,6),
    QuantitąGiacenza			numeric(19,6),
    Magazzino					nvarchar(100),
    Ubicazione					nvarchar(100),
    CodiceBP					nvarchar(100),
    NumeroOrdine				nvarchar(100),
    IDinternoDocumento			INT,
    NomeBP						nvarchar(100)

)


	set @Ordine=0

declare  cur_temp cursor for
	select	Code,
			U_FO_ARTICOLO,
			U_FO_BP,
			U_FO_DOCENTRY,
			U_FO_NOME_BP,
			U_FO_NUM_ORD,
			U_FO_QNT_RIGA
	from "@FO_ARTICOLO_BP"
	where	Name=@sequence 
	order by Code

open cur_temp

fetch next from cur_temp into @Code, @Articolo,@CodiceBP,@IDinternoDocumento,@NomeBP,@NumeroOrdine,@QtaRiga
while @@fetch_status=0
begin

	set @Ordine = @Ordine+1

	select @cont = count(*) from oitt T0 where T0.Code = @Articolo
	if @cont=0
		begin
			insert into #TMP_DIBA 
			select	@Ordine,
					T2.ItmsGrpNam as "Gruppo articolo", 
					T0.ItemCode as "Articolo", 
					T0.ItemName as "Descrizione", 
					1 * @QtaRiga as "Quantitą totale Necessaria" ,  
					t3.OnHand as "Quantitą in giacenza", 
					t3.WhsCode as "Magazzino", 
					T8.BinCode as "Ubicazione", 
					@CodiceBP as "Codice BP", 
					@NumeroOrdine as "Numero Ordine", 
					@IDinternoDocumento AS "ID interno documento", 
					@NomeBP AS "Nome BP"    
			FROM	OITM T0 
						INNER JOIN OITB T2 ON T2.ItmsGrpCod = T0.ItmsGrpCod  
						INNER JOIN OITW T3 ON T3.ItemCode = T0.ItemCode  AND T3.OnHand > 0 
						LEFT JOIN OIBQ T7 ON T0.ItemCode = T7.ItemCode AND T7.OnHandQty > 0   
						LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs"   
			WHERE	T0.ItemCode =  @Articolo AND
					NOT EXISTS ( SELECT 1 FROM OITT T1 WHERE T0.ItemCode = T1.Code )  
		end
	
	if @cont!=0
		begin
			;WITH cte AS
			(
				SELECT	distinct NULL as parent, 
						T0.Code as child, 
						1 as level ,
						T0.Qauntity,
						T0.ToWH, 
						@CodiceBP as "Codice BP", 
						@NumeroOrdine as "Numero Ordine", 
						@IDinternoDocumento AS "ID interno documento", 
						@NomeBP AS "Nome BP"  ,  
 						@QtaRiga  as "Qta riga"
				FROM	OITT T0 
				WHERE	T0.Code= @Articolo
				UNION ALL
				SELECT	T0.Father as parent, 
						T0.Code as child, 
						1 as level ,
						T0.Quantity,
						T0.Warehouse, 
						@CodiceBP as "Codice BP", 
						@NumeroOrdine as "Numero Ordine", 
						@IDinternoDocumento AS "ID interno documento", 
						@NomeBP AS "Nome BP"  ,  
 						@QtaRiga  as "Qta riga"
				FROM	ITT1 T0 
				WHERE	T0.Father= @Articolo
				UNION ALL
				SELECT	T0.Father, 
						T0.Code , 
						(c.level + 1),
						T0.Quantity,
						T0.Warehouse, 
						@CodiceBP as "Codice BP", 
						@NumeroOrdine as "Numero Ordine", 
						@IDinternoDocumento AS "ID interno documento", 
						@NomeBP AS "Nome BP"  ,  
 						@QtaRiga  as "Qta riga"
				FROM	ITT1 T0  
							INNER JOIN cte c ON T0.Father = c.child
				WHERE	c.parent is not  null
				)
			insert into #TMP_DIBA 
			SELECT	@Ordine,
					T2.ItmsGrpNam as "Gruppo articolo", 
 					T0.child as "Articolo", 
 					T1.ItemName as "Descrizione", 
 					T0.Qauntity * T0.[Qta riga] as "Quantitą totale Necessaria" , 
 					t3.OnHand as "Quantitą in giacenza", 
 					t3.WhsCode as "Magazzino", 
 					T8.BinCode as "Ubicazione", 
					@CodiceBP as "Codice BP", 
					@NumeroOrdine as "Numero Ordine", 
					@IDinternoDocumento AS "ID interno documento", 
					@NomeBP AS "Nome BP"  
			FROM	CTE T0 
						INNER JOIN OITM T1 ON T1.ItemCode = T0.child 
 						INNER JOIN OITB T2 ON T2.ItmsGrpCod = T1.ItmsGrpCod  
 						INNER JOIN OITW T3 ON T3.ItemCode = T1.ItemCode AND T3.WhsCode = 'Mc'
 						LEFT JOIN OIBQ T7 ON T1.ItemCode = T7.ItemCode AND T7.OnHandQty > 0 and T7.WhsCode = T3.WhsCode  
 						LEFT JOIN OBIN T8 ON T8."AbsEntry" = T7."BinAbs" and T7."WhsCode" = T3."WhsCode" 
			order by level,parent+child			
		end
	fetch next from cur_temp into @Code, @Articolo,@CodiceBP,@IDinternoDocumento,@NomeBP,@NumeroOrdine,@QtaRiga   
end

close cur_temp
deallocate cur_temp 

select * from #TMP_DIBA 

END


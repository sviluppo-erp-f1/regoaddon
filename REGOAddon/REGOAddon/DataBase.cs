﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManager;

namespace REGOAddon
{
    class DataBase : Db
    {
        public DataBase()
        {
            Tables = new DbTable[] {
                new DbTable("@ADDONPAR", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),
                new DbTable("@FO_ARTICOLO_BP", "Articolo-BP-Doc", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement),
                new DbTable("@FO_APPR_FAMPROD", "Approntamento fam-prod", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement),
            };

            Columns = new DbColumn[] {
                
                #region FOADDONPAR Parametri addon
                new DbColumn("@ADDONPAR", "VALOREDEC", "Valore decimale",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10, new DbValidValue[0],-1,null),
                  new DbColumn("@ADDONPAR", "VALORE", "Valore",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 250, new DbValidValue[0],-1,null),
                #endregion
                    
              #region FO_ARTICOLO_BP
                new DbColumn("@FO_ARTICOLO_BP", "FO_BP", "Business Partner",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50,new DbValidValue[0],-1,null),
                new DbColumn("@FO_ARTICOLO_BP", "FO_ARTICOLO", "Articolo",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50,new DbValidValue[0],-1,null),
                new DbColumn("@FO_ARTICOLO_BP", "FO_NUM_ORD", "Numero Ordine",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25,new DbValidValue[0],-1,null),
                new DbColumn("@FO_ARTICOLO_BP", "FO_NOME_BP", "Nome BP",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254,new DbValidValue[0],-1,null),
                new DbColumn("@FO_ARTICOLO_BP", "FO_DOCENTRY", "ID Interno Doc",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 25,new DbValidValue[0],-1,null),
                new DbColumn("@FO_ARTICOLO_BP", "FO_QNT_RIGA", "Quantità Riga",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 25,new DbValidValue[0],-1,null),
                #endregion
                
              #region FO_APPR_FAMPROD
                new DbColumn("@FO_APPR_FAMPROD", "FO_CODFAM", "Codice famiglia",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 50,new DbValidValue[0],-1,null),
                new DbColumn("@FO_APPR_FAMPROD", "FO_DESFAM", "Descrizione famiglia",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100,new DbValidValue[0],-1,null),
                new DbColumn("@FO_APPR_FAMPROD", "FO_NUM_VAR", "Variabile",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10,new DbValidValue[0],-1,null),
                new DbColumn("@FO_APPR_FAMPROD", "FO_NUM_COST", "Costante",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Price, 10,new DbValidValue[0],-1,null),               
                #endregion

               new DbColumn("OITB", "FO_LISTINO", "Listino prezzi",
                    SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100,new DbValidValue[0],-1,null),

               new DbColumn("ORDR", "FO_TEMPOPREP", "Tempo preparazione",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10,new DbValidValue[0],-1,null),
               new DbColumn("ORDR", "FO_DATAAPPR", "Data approntamento",
                    SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10,new DbValidValue[0],-1,null),
               new DbColumn("RDR1", "FO_TEMPOPREP", "Tempo preparazione",
                    SAPbobsCOM.BoFieldTypes.db_Float, SAPbobsCOM.BoFldSubTypes.st_Quantity, 10,new DbValidValue[0],-1,null),
               new DbColumn("RDR1", "FO_DATAAPPR", "Data approntamento",
                    SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10,new DbValidValue[0],-1,null),

            };

        }
    }
}

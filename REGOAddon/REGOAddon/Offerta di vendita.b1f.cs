
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace REGOAddon
{

    [FormAttribute("149", "Offerta di vendita.b1f")]
    class Offerta_di_vendita : SystemFormBase
    {
        public Offerta_di_vendita()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_1").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            this.EditText0.DataBind.SetBound(true, "OQUT", "U_FO_DATAPREVISTA");
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string data = "";
            string dataMax = "";
            bool callPrc = false;
            //Cancello la tabella temporanea
            oRecordset.DoQuery("DELETE FROM TMP_DOC_IN_ELABORAZIONE");

            SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)this.GetItem("38").Specific;
            for (int row = 1; row <= oMatrix.RowCount; row++)
            {
                if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1").Cells.Item(row).Specific)).Value.ToString() != "")
                {
                    try
                    {

                        string sqlInsert = "";
                        sqlInsert += "INSERT INTO TMP_DOC_IN_ELABORAZIONE (";
                        sqlInsert += "  \"ItemCode\", ";
                        sqlInsert += "  \"ShipDate\", ";
                        sqlInsert += "  \"DocEntry\", ";
                        sqlInsert += "  \"DocNum\", ";
                        sqlInsert += "  \"LineNum\", ";
                        sqlInsert += "  \"CardCode\", ";
                        sqlInsert += "  \"CardName\", ";
                        sqlInsert += "  \"Quantity\", ";
                        sqlInsert += "  \"Fonte\", ";
                        sqlInsert += "  \"DocDueDate\" ";
                        sqlInsert += ") VALUES (";
                        sqlInsert += "  '" + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1").Cells.Item(row).Specific)).Value.ToString() + "', ";
                        if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("25").Cells.Item(row).Specific)).Value.ToString() != "")
                        {
                            sqlInsert += "  '" + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("25").Cells.Item(row).Specific)).Value.ToString() + "', ";
                        }
                        else
                        {
                            sqlInsert += "  CURRENT_DATE, ";
                        }

                        sqlInsert += "  0, ";
                        sqlInsert += "  0, ";
                        sqlInsert += "  " + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("110").Cells.Item(row).Specific)).Value.ToString() + ", ";
                        sqlInsert += "  '" + ((SAPbouiCOM.EditText)(this.GetItem("4").Specific)).Value.ToString() + "', ";
                        sqlInsert += "  null, ";
                        sqlInsert += "  " + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("11").Cells.Item(row).Specific)).Value.ToString().Replace(",", ".") + ", ";
                        sqlInsert += "  'app', ";
                        sqlInsert += "  null ";
                        sqlInsert += ") ";

                        oRecordset.DoQuery(sqlInsert);
                        callPrc = true;
                    }
                    catch (Exception e)
                    {
                        callPrc = false;
                        Application.SBO_Application.MessageBox("Si è verificato un errore in fase di calcolo: " + e.Message);
                    }

                }
            }

            if (callPrc)
            {

                oRecordset.DoQuery("CALL RigheOrdiniPerAddOn()");
                while (!oRecordset.EoF)
                {

                    string d = oRecordset.Fields.Item(1).Value.ToString();
                    data = d.Substring(6, 4) + d.Substring(3, 2) + d.Substring(0, 2);

                    if (dataMax == "")
                    {
                        this.EditText0.Value = data;
                        dataMax = data;
                    }

                    for (int row = 1; row <= oMatrix.RowCount; row++)
                    {
                        if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("110").Cells.Item(row).Specific)).Value.ToString() == oRecordset.Fields.Item(0).Value.ToString())
                        {
                            ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("U_FO_DATAPREVISTA").Cells.Item(row).Specific)).Value = data;
                            break;
                        }
                    }

                    oRecordset.MoveNext();
                }

                Application.SBO_Application.MessageBox("Calcolo data completato");
            }

        }

        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.Button0.Item.Top = this.GetItem("4").Top;
            this.Button0.Item.Left = this.GetItem("4").Left + this.GetItem("4").Width +20;

        }
    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace REGOAddon
{

    [FormAttribute("139", "Ordine cliente.b1f")]
    class Ordine_cliente : SystemFormBase
    {
        public Ordine_cliente()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private SAPbouiCOM.Button Button0;

        private void OnCustomInitialize()
        {
            this.EditText0.DataBind.SetBound(true, "ORDR", "U_FO_TEMPOPREP");
            this.EditText1.DataBind.SetBound(true, "ORDR", "U_FO_DATAAPPR");
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string data = "";
            string dataA = "";
            string dataMax = "";
            bool callPrc = false;
            //Cancello la tabella temporanea
            oRecordset.DoQuery("DELETE FROM TMP_DOC_IN_ELABORAZIONE");

            this.UIAPIRawForm.Freeze(true);
            SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)this.GetItem("38").Specific;
            for (int row = 1; row <= oMatrix.RowCount; row++)
            {
                if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1").Cells.Item(row).Specific)).Value.ToString() != "")
                {
                    try
                    {

                        string sqlInsert = "";
                        sqlInsert += "INSERT INTO TMP_DOC_IN_ELABORAZIONE (";
                        sqlInsert += "  \"ItemCode\", ";
                        sqlInsert += "  \"ShipDate\", ";
                        sqlInsert += "  \"DocEntry\", ";
                        sqlInsert += "  \"DocNum\", ";
                        sqlInsert += "  \"LineNum\", ";
                        sqlInsert += "  \"CardCode\", ";
                        sqlInsert += "  \"CardName\", ";
                        sqlInsert += "  \"Quantity\", ";
                        sqlInsert += "  \"Fonte\", ";
                        sqlInsert += "  \"DocDueDate\" ";
                        sqlInsert += ") VALUES (";
                        sqlInsert += "  '" + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1").Cells.Item(row).Specific)).Value.ToString() + "', ";
                        if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("25").Cells.Item(row).Specific)).Value.ToString() !="")
                        {
                            sqlInsert += "  '" + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("25").Cells.Item(row).Specific)).Value.ToString() + "', ";
                        }
                        else
                        {
                            sqlInsert += "  CURRENT_DATE, ";
                        }
                        
                        sqlInsert += "  0, ";
                        sqlInsert += "  0, ";
                        sqlInsert += "  " + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("110").Cells.Item(row).Specific)).Value.ToString() + ", ";
                        sqlInsert += "  '" + ((SAPbouiCOM.EditText)(this.GetItem("4").Specific)).Value.ToString() + "', ";
                        sqlInsert += "  null, ";
                        sqlInsert += "  " + ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("11").Cells.Item(row).Specific)).Value.ToString().Replace(",", ".") + ", ";
                        sqlInsert += "  'app', ";
                        sqlInsert += "  null ";
                        sqlInsert += ") ";

                        oRecordset.DoQuery(sqlInsert);
                        callPrc = true;
                        
                    }
                    catch (Exception e)
                    {
                        callPrc = false;
                        this.UIAPIRawForm.Freeze(false);
                        Application.SBO_Application.MessageBox("Si è verificato un errore in fase di calcolo: " + e.Message);
                        return;
                    }
                    
                }
            }

            double tempoPreparazioneTot = 0;
            double tempoPreparazione = 0;
            
            if (callPrc)
            {
                oRecordset.DoQuery("CALL RigheOrdiniPerAddOn()");
                while (!oRecordset.EoF)
                {
                    //xxx += 1;
                    //string d = oRecordset.Fields.Item(1).Value.ToString();
                    //data = d.Substring(6, 4) + d.Substring(3, 2) + d.Substring(0, 2);

                    //string da = oRecordset.Fields.Item(2).Value.ToString();
                    //dataA = da.Substring(6, 4) + da.Substring(3, 2) + da.Substring(0, 2);

                    //if (dataMax == "")
                    //{
                    //    ((SAPbouiCOM.EditText)(this.GetItem("12").Specific)).Value = data;
                    //    dataMax = data;
                    //}

                    ((SAPbouiCOM.EditText)(this.GetItem("12").Specific)).Value = recuperaMaxDataConsegna();
                    this.EditText1.Value = recuperaMaxDataApprontamento();

                    double totaleDoc = totaleDocumento();
                    for (int row = 1; row <= oMatrix.RowCount; row++)
                    {
                        if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("1").Cells.Item(row).Specific)).Value.ToString() != "")
                        {
                                //string sql = $@"	SELECT 	TA.""VisOrder"" AS ""Riga Ordine"",
                                //                COALESCE(TA.""DataConsegna"", (SELECT MAX(TAA.""DataConsegna"") FROM ""TMP_DOC_ELAB"" TAA WHERE   TAA.""ItemCode"" = TA.""ItemCode"")) AS ""Data"",
                                //       COALESCE(TA.""DataApprontamento"", (SELECT MAX(TAA.""DataApprontamento"") FROM ""TMP_DOC_ELAB"" TAA WHERE	TAA.""ItemCode"" = TA.""ItemCode"")) AS ""DataApprontamento""
                                //        FROM    ""TMP_DOC_ELAB"" TA
                                //                    INNER JOIN OITM TB ON TA.""ItemCode"" = TB.""ItemCode""
                                //                    LEFT OUTER JOIN ""@FO_GG_FAMPROD"" TC ON TB.""U_FamProd"" = TC.U_FO_FAMPROD
                                //        WHERE   TA.""VisOrder"" = {((SAPbouiCOM.EditText)(oMatrix.Columns.Item("110").Cells.Item(row).Specific)).Value.ToString()} ";
                                //oRecordset.DoQuery(sql);



                                if (((SAPbouiCOM.EditText)(oMatrix.Columns.Item("110").Cells.Item(row).Specific)).Value.ToString() == oRecordset.Fields.Item(0).Value.ToString())
                                {
                                    string d = oRecordset.Fields.Item(1).Value.ToString();
                                    data = d.Substring(6, 4) + d.Substring(3, 2) + d.Substring(0, 2);

                                    string da = oRecordset.Fields.Item(2).Value.ToString();
                                    dataA = da.Substring(6, 4) + da.Substring(3, 2) + da.Substring(0, 2);

                                    ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("25").Cells.Item(row).Specific)).Value = data;
                                    ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("U_FO_DATAAPPR").Cells.Item(row).Specific)).Value = dataA;

                                    tempoPreparazione = calcolaTempoPreparazione(oRecordset.Fields.Item(0).Value.ToString(), totaleRiga(row, oMatrix), totaleDoc);
                                    int tempoPreparazioneIntero = int.Parse(Math.Ceiling(tempoPreparazione).ToString());
                                    tempoPreparazioneTot += tempoPreparazioneIntero;
                                    ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("U_FO_TEMPOPREP").Cells.Item(row).Specific)).Value = tempoPreparazioneIntero.ToString().Replace(",", ".");
                                    break;
                                }
                            }
                    }

                    oRecordset.MoveNext();
                }
                this.EditText0.Value = tempoPreparazioneTot.ToString().Replace(",", ".");


                Application.SBO_Application.MessageBox("Calcolo data completato");
            }
            this.UIAPIRawForm.Freeze(false);
        }

        private double totaleDocumento()
        {
            //Recupero Totale documento
            double imponibile = 0;
            int posImponibile = 0;
            posImponibile = ((SAPbouiCOM.EditText)(this.GetItem("22").Specific)).Value.ToString().IndexOf(" ", 0);
            if (posImponibile != -1)
            {
                imponibile = double.Parse(((SAPbouiCOM.EditText)(this.GetItem("22").Specific)).Value.ToString().Substring(0, posImponibile));
            }

            //Recupero sconto documento
            double sconto = 0;
            int posSconto = 0;
            posSconto = ((SAPbouiCOM.EditText)(this.GetItem("42").Specific)).Value.ToString().IndexOf(" ", 0);
            if (posSconto != -1)
            {
                sconto = double.Parse(((SAPbouiCOM.EditText)(this.GetItem("42").Specific)).Value.ToString().Substring(0, posSconto));
            }

            return  imponibile - sconto;
        }

        private double totaleRiga(int row, SAPbouiCOM.Matrix oMatrix)
        {
            double imponibile = 0;
            int posImponibile = 0;
            
            posImponibile = ((SAPbouiCOM.EditText)(oMatrix.Columns.Item("21").Cells.Item(row).Specific)).Value.ToString().IndexOf(" ", 0);
            if (posImponibile != -1)
            {
                imponibile = double.Parse(((SAPbouiCOM.EditText)(oMatrix.Columns.Item("21").Cells.Item(row).Specific)).Value.ToString().Substring(0, posImponibile));
            }

            return imponibile;
        }

        private string recuperaMaxDataApprontamento()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string sqlSelect = $@"SELECT MAX(""DataApprontamento"") AS ""data"" FROM TMP_DOC_ELAB " ;
            oRecordset.DoQuery(sqlSelect);
            string data = oRecordset.Fields.Item(0).Value.ToString();
            return data.Substring(6, 4) + data.Substring(3, 2) + data.Substring(0, 2); ;
        }

        private string recuperaMaxDataConsegna()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string sqlSelect = $@"SELECT MAX(""DataConsegna"") AS ""data"" FROM TMP_DOC_ELAB ";
            oRecordset.DoQuery(sqlSelect);
            string data = oRecordset.Fields.Item(0).Value.ToString();
            return data.Substring(6, 4) + data.Substring(3, 2) + data.Substring(0, 2); ;
        }

        private double calcolaTempoPreparazione(string lineNum, double totaleRiga, double totaleDocumento)
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //Estraggo la variante e la costante
            string sqlSelect = $@"SELECT  DISTINCT c.U_FO_NUM_VAR, c.U_FO_NUM_COST, c.U_FO_CODFAM " +
                                $@"FROM    TMP_DOC_IN_ELABORAZIONE a " +
                                $@"             INNER JOIN OITM b ON a.""ItemCode"" = b.""ItemCode"" " +
                                $@"             INNER JOIN ""@FO_APPR_FAMPROD"" c ON c.U_FO_CODFAM = b.""U_FamProd"" " +
                                $@"WHERE   a.""LineNum"" = {lineNum} ";
            oRecordset.DoQuery(sqlSelect);
            double variabile = double.Parse(oRecordset.Fields.Item(0).Value.ToString());
            double costante = double.Parse(oRecordset.Fields.Item(1).Value.ToString());
            string codiceFamiglia = oRecordset.Fields.Item(2).Value.ToString();
            if (codiceFamiglia=="")
            {
                return 0;
            }

            ////Estraggo il numero di articoli della stessa famiglia
            //sqlSelect = $@"SELECT  COUNT(*) AS ""cnt"" " +
            //        $@"FROM   TMP_DOC_IN_ELABORAZIONE a " +
            //        $@"             INNER JOIN OITM b ON a.""ItemCode"" = b.""ItemCode"" " +
            //        $@"             INNER JOIN ""@FO_APPR_FAMPROD"" c ON c.U_FO_CODFAM = b.""U_FamProd"" " +
            //        $@"WHERE  c.U_FO_CODFAM = '{codiceFamiglia}' ";
            //oRecordset.DoQuery(sqlSelect);

            //int numeroArticoli = int.Parse(oRecordset.Fields.Item(0).Value.ToString());

            //double tempo = (totaleDocumento * variabile) + (costante / numeroArticoli);

            double tempo = (totaleRiga * variabile) +(totaleRiga / totaleDocumento) * costante;


            return tempo;
        } 

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.Button0.Item.Top = this.GetItem("4").Top;
            this.Button0.Item.Left = this.GetItem("4").Left + this.GetItem("4").Width + 20;

        }

        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.StaticText StaticText1;


    }
}

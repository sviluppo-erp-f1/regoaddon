﻿using System;
using System.Collections.Generic;
using System.Xml;
using SAPbouiCOM.Framework;

namespace REGOAddon
{
    [FormAttribute("REGOAddon.RigheOrdineCliente", "RigheOrdineCliente.b1f")]
    class RigheOrdineCliente : UserFormBase
    {
        public RigheOrdineCliente()
        {
        }

        #region items
        private SAPbouiCOM.Grid Grid0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Grid Grid1;
        private SAPbouiCOM.Button Button1;
        #endregion

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Grid1 = ((SAPbouiCOM.Grid)(this.GetItem("Item_2").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.OnCustomInitialize();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private void OnCustomInitialize()
        {
            populateGrid0();
        }

        private void populateGrid0()
        {
            string q = "SELECT  T1.\"DocDate\" as \"Data Documento\", T1.\"DocEntry\", T1.\"DocNum\" as \"Numero Documento\" , T1.\"CardCode\" as \"Codice Cliente\", T1.\"CardName\" as \"Nome Cliente\",";
            q += " T0.\"ItemCode\" as \"Codice Articolo\", T0.\"Dscription\" as \"Descrizione\", TO_VARCHAR(T0.\"ShipDate\",'mm-dd-yyyy')   as \"Data di Consegna\" ,T0.\"WhsCode\" as \"Magazzino\" ,T0.\"Quantity\" as \"Quantità\", T0.\"OpenQty\" AS \"Quantità Aperta\" ";
            q += " FROM RDR1 T0 INNER JOIN ORDR T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" "; //inner join OITT T2 ON T2.Code = T0.ItemCode 
            q += " WHERE T0.\"LineStatus\"  = 'O' ";
            this.Grid0.DataTable.ExecuteQuery(q);

            this.Grid0.Columns.Item("DocEntry").Visible = false;

            for (int j = 0; j <= this.Grid0.Columns.Count - 1; j++)
            {
                this.Grid0.Columns.Item(j).Editable = false;
                this.Grid0.Columns.Item(j).TitleObject.Sortable = true;
            }
            this.Grid0.AutoResizeColumns();
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try { 
            this.Grid1.DataTable.Clear();
            this.UIAPIRawForm.Freeze(true);

            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string sequence = Program.oCompany.UserSignature + "_" + DateTime.Now.ToString();
            for (int i = 0; i < this.Grid0.Rows.SelectedRows.Count; i++)
            {
                int iSelectedRow = this.Grid0.Rows.SelectedRows.Item(i, SAPbouiCOM.BoOrderType.ot_RowOrder);

                string ItemCode = this.Grid0.DataTable.GetValue("Codice Articolo", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();
                string DocNum = this.Grid0.DataTable.GetValue("Numero Documento", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();
                string DocEntry = this.Grid0.DataTable.GetValue("DocEntry", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();
                string CardCode = this.Grid0.DataTable.GetValue("Codice Cliente", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();
                string CardName = this.Grid0.DataTable.GetValue("Nome Cliente", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();
                string qntRigaAperta = this.Grid0.DataTable.GetValue("Quantità Aperta", this.Grid0.GetDataTableRowIndex(iSelectedRow)).ToString();

                //string ItemCode = this.Grid0.DataTable.Columns.Item("Codice Articolo").Cells.Item(iSelectedRow).Value.ToString();
                //string DocNum = this.Grid0.DataTable.Columns.Item("Numero Documento").Cells.Item(iSelectedRow).Value.ToString();
                //string DocEntry = this.Grid0.DataTable.Columns.Item("DocEntry").Cells.Item(iSelectedRow).Value.ToString();
                //string CardCode = this.Grid0.DataTable.Columns.Item("Codice Cliente").Cells.Item(iSelectedRow).Value.ToString();
                //string CardName = this.Grid0.DataTable.Columns.Item("Nome Cliente").Cells.Item(iSelectedRow).Value.ToString();
                //string qntRigaAperta = this.Grid0.DataTable.Columns.Item("Quantità Aperta").Cells.Item(iSelectedRow).Value.ToString();

                string code = "";
                string qcode = "SELECT \"@FO_ARTICOLO_BP_S\".NEXTVAL FROM DUMMY";
                 oRecordset.DoQuery(qcode);
                while (!oRecordset.EoF)
                {
                    code = oRecordset.Fields.Item(0).Value.ToString();
                    oRecordset.MoveNext();
                }
                string qInsert = "INSERT INTO \"@FO_ARTICOLO_BP\" (\"Code\",\"Name\", U_FO_BP, U_FO_NOME_BP, U_FO_DOCENTRY, U_FO_NUM_ORD, U_FO_ARTICOLO, U_FO_QNT_RIGA)";
                qInsert += " VALUES('"+code+"', '" + sequence + "', '" + CardCode.Replace("'", "''") + "', '" + CardName.Replace("'", "''") + "', '" + DocEntry.Replace("'", "''") + "','" + DocNum.Replace("'", "''") + "','" + ItemCode.Replace("'", "''") + "','" + qntRigaAperta.Replace("'", "''") + "'); ";
                oRecordset.DoQuery(qInsert);
            }



            string q = "CALL prcReport ('" + sequence + "') ";
            this.Grid1.DataTable.ExecuteQuery(q);
            this.Grid1.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);

            for (int j = 0; j <= this.Grid1.Columns.Count - 1; j++)
            {
                this.Grid1.Columns.Item(j).Editable = false;
                this.Grid1.Columns.Item(j).TitleObject.Sortable = true;
            }

            string qDelete = " DELETE FROM \"@FO_ARTICOLO_BP\" WHERE \"Name\" = '" + sequence + "'";
            oRecordset.DoQuery(qDelete);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Errore " + e.Message);
            }
        }



        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.Grid0.Item.Width = this.UIAPIRawForm.Width - 25;
            this.Grid1.Item.Width = this.UIAPIRawForm.Width - 25;

            this.Grid0.Item.Height = this.UIAPIRawForm.Height / 2 - 67;
            this.Grid1.Item.Height = this.UIAPIRawForm.Height / 2 - 67;

            this.Grid0.Item.Left = 10;
            this.Grid0.Item.Top = 10;
            //griglia altezza e dimensione
            this.Button0.Item.Left = this.Grid0.Item.Left;
            this.Button0.Item.Top = this.Grid0.Item.Top + this.Grid0.Item.Height + 10;
            this.Grid1.Item.Left = this.Button0.Item.Left;
            this.Grid1.Item.Top = this.Button0.Item.Top + this.Button0.Item.Height + 10;
            this.Button1.Item.Left = this.Grid1.Item.Left + this.Grid1.Item.Width - this.Button1.Item.Width - 25;
            this.Button1.Item.Top = this.Grid1.Item.Top + this.Grid1.Item.Height + 5;
        }
    }
}